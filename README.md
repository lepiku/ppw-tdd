PPW TDD
============

[![Pipeline](https://gitlab.com/lepiku/ppw-tdd/badges/master/pipeline.svg)](https://gitlab.com/lepiku/ppw-tdd/pipelines)
[![Coverage](https://gitlab.com/lepiku/ppw-tdd/badges/master/coverage.svg)](https://gitlab.com/lepiku/ppw-tdd/)

Ini adalah project untuk story 6 PPW.

## Setup

```bash
git clone "https://gitlab.com/lepiku/ppw-tdd.git"
cd ppw-tdd
wget "https://chromedriver.storage.googleapis.com/77.0.3865.40/chromedriver_linux64.zip"
unzip chromedriver_linux64.zip

python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

__Note:__ Kamu butuh Google Chrome atau Chromium untuk menjalankan test.
