from django.test import LiveServerTestCase, TestCase
from django.utils import timezone
from selenium import webdriver

from .models import Status


class Story6Test(TestCase):
    def test_index_show_form(self):
        response = self.client.get('/')

        # assertContains sudah mengecek status code, jadi tidak perlu dicek lagi
        self.assertContains(response, 'Halo, apa kabar?', html=True)
        self.assertContains(response, '<form')
        self.assertContains(
            response,
            '<input type="text" name="status" id="id_status" maxlength="300" required>',
            html=True,
        )
        self.assertContains(response, 'type="submit"')

    def test_index_post(self):
        response = self.client.post('/', follow=True, data={
            'status': 'Lagi gabut',
        })

        self.assertRedirects(response, '/')
        self.assertEqual(Status.objects.count(), 1)

        today = timezone.now()
        status = Status.objects.first()

        self.assertEqual(status.waktu_dibuat.date(), today.date())

    def test_index_show_status(self):
        response = self.client.post('/', follow=True, data={
            'status': 'Lagi gabut',
        })

        self.assertContains(response, 'Lagi gabut', html=True)

class Story6LiveTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_index(self):
        self.selenium.get(self.live_server_url + '/')

        self.assertInHTML('Halo, apa kabar?', self.selenium.page_source)

        status = self.selenium.find_element_by_id('id_status')
        button = self.selenium.find_element_by_tag_name('button')

        status.send_keys('Coba Coba')
        button.click()

        self.assertInHTML('Coba Coba', self.selenium.page_source)
