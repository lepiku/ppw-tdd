from django.urls import path
from . import views

app_name = 'story_6'

urlpatterns = [
    path('', views.index, name='index'),
]
