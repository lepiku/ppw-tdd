from django.db import models


class Status(models.Model):
    status = models.CharField(max_length=300)
    waktu_dibuat = models.DateTimeField(auto_now_add=True)
